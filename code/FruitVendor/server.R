#

library(shiny)
library(ggplot2)

prior.demand.mean <- 50
prior.demand.sd <- 20

# options: normal, poisson
demand.distribution <- "poisson"

# Define server logic required to draw a histogram
shinyServer(function(input, output, session) {
    
    # generate a default demand, so the simulation can be run even without generating it manually
    # initialize them globally for the function
    demand.mean <- NULL 
    demand.sd <- NULL 
    demand.sample <- c()
    
    simulation.result <- reactiveValues(data = data.frame(Day = c(), Wealth = c(), Simulation = c()))
    
    # samples values from the demand function
    sample.demand <- function(count){
        if(demand.distribution == "normal"){
            return(pmax(0,round(rnorm(count, demand.mean, demand.sd))))
        }else if(demand.distribution == "poisson"){
            return(pmax(0,round(rpois(count, demand.mean))))
        }else{
            stop("Unknown demand distribution!")
        }
    }
    
    # generate new true demand
    generate.demand <- function(seed, sampleCount){
        # set the seed for the reproducibility of the generated demand
        set.seed(seed)
        demand.mean <<- rnorm(1, prior.demand.mean, prior.demand.sd)
        demand.sd <<- demand.mean / 3
        
        cat("New mean demand: ", demand.mean, "\n")
        demand.sample <<- sample.demand(sampleCount)
        sample.data <- data.frame(Demand = demand.sample)
        output$observeddemands <- renderTable(sample.data)
        
        # reset simulation results
        simulation.result$data <- data.frame(Day = c(), Wealth = c(), Simulation = c())
        set.seed(NULL)
    }
    
    # these values should come from the input instead
    generate.demand(NULL, 3)
    
    # Generate new demand values when pressed
    observeEvent(input$generatedemand, {
            if ( is.integer(input$seed) ) {
                seed <- input$seed
            } else {
                seed <- NULL
            }
            generate.demand(seed, input$demandSamples)
        }
    )
    
    # Run simulation
    observeEvent(input$simulate, {
        withProgress(message = "Running simulation", value = 0, {            
            simulation.length <- input$horizon
            simulation.epochs <- input$epochs
            p <- input$price
            c <- input$cost
            runname <- paste("Order =", input$order)
            
            sim.results <- matrix(nrow = simulation.length, ncol = simulation.epochs)
            
            for (epoch in 1:simulation.epochs) {
                if (epoch %% 100 == 0)
                    incProgress(100/simulation.epochs)
                
                demands <- sample.demand(simulation.length)
                orders <- rep(input$order,simulation.length)
                sold <- pmin(demands,orders)
                wealth <- cumsum(sold * p - c * orders )   
                
                sim.results[,epoch] <- wealth
            }
          
            wealth.mean <- apply(sim.results, 1, mean)
            wealth.diff <- apply(sim.results, 1, sd) / sqrt(epoch)
            
            # remove the data from the previous run
            if (nrow(simulation.result$data) > 0) {
                simulation.result$data <- 
                    simulation.result$data[!simulation.result$data$Simulation == runname,]
            }
            
            # append results to the general storage
            simulation.result$data <- rbind(simulation.result$data, data.frame(
                Day = 1:simulation.length,
                Wealth = wealth.mean,
                Wealth.Min = wealth.mean - wealth.diff,
                Wealth.Max = wealth.mean + wealth.diff,
                Simulation = runname))  
        })
    })
    
    # resets simulation
    observeEvent(input$resetsimulation,{
        cat("Resetting simulation")
        simulation.result$data <- data.frame(Day = c(), Wealth = c(), Simulation = c())
    })
    
    # plot money in the bank
    output$money <- renderPlot({
        if (nrow(simulation.result$data) > 0) {
            ggplot(simulation.result$data, aes(
                    x = Day, y = Wealth, ymin = Wealth.Min, ymax = Wealth.Max, color = Simulation, group = Simulation, fill = Simulation)) +
                geom_line()  +
                geom_ribbon(alpha = 0.3) +
                theme_linedraw(base_size = 20)
        }
    }, height = 500)
    
    # compute optimal threshold
    observeEvent(input$computeOptimal, {
        data <- demand.sample
        demand.mean.est <- mean(data)
        demand.sd.est <- sd(data)
        
        cost.buy <- input$cost
        price.sell <- input$price
        #cat("Cost:", cost.buy, "Price:", price.sell, "\n")
        
        if(demand.distribution == "normal"){
            # True solution
            q.true <- as.integer(round(qnorm((price.sell - cost.buy)/price.sell, demand.mean, demand.sd)))    
            # Model-based solution 
            q.est.model <- as.integer(round(qnorm((price.sell - cost.buy)/price.sell, demand.mean.est, demand.sd.est)))
        }else if(demand.distribution == "poisson"){
            # True solution
            q.true <- as.integer(round(qpois((price.sell - cost.buy)/price.sell, demand.mean)))    
            # Model-based solution 
            q.est.model <- as.integer(round(qpois((price.sell - cost.buy)/price.sell, demand.mean.est)))    
        }else{
            stop("Unknown demand distribution!")            
        }
        
        # Model-free solution
        candidates <- 1:100
        values <- 
            sapply(candidates, function(q){
                mean(vapply(data, function(d){
                    price.sell*min(d,q)- cost.buy*q
                },0))
            })
        q.est.free <- candidates[which.max(values)]
        
        output.data <- data.frame(labels = c("Demand mean", "True mean", 
                                             "Demand sd", "True sd", 
                                             "Estimated model-based", 
                                             "Estimated model-free",
                                             "True opt order"),
                                 values = c(demand.mean.est, demand.mean, 
                                            demand.sd.est, demand.sd, 
                                            q.est.model, 
                                            q.est.free,
                                            q.true  ))
        
        showModal(modalDialog(
            title = "Optimal Threshold (estimated model)",
            easyClose = TRUE,
            renderTable(output.data, colnames = FALSE )
        ))
        
    })
    
    # demands download handler
    output$downloadDemands <- downloadHandler(
        filename = "demands.csv",
        content = function(file){
            write.csv(data.frame(Demand = demand.sample), file, row.names = FALSE)
        }
    )
    
})
